#include <gtest/gtest.h>

#include <chrono>
#include <thread>
#include <vector>
#include <atomic>

#include <mpmc.h>

using namespace std::chrono;

TEST(Correctness, Enqueue) {
    MPMCBoundedQueue<int> queue(2);
    ASSERT_TRUE(queue.enqueue(2));
    ASSERT_TRUE(queue.enqueue(2));
    ASSERT_FALSE(queue.enqueue(2));
    ASSERT_FALSE(queue.enqueue(2));
}

TEST(Correctness, Dequeue) {
    int val;
    MPMCBoundedQueue<int> queue(2);
    ASSERT_FALSE(queue.dequeue(val));
    ASSERT_FALSE(queue.dequeue(val));
}

TEST(Correctness, EnqueueDequeue) {
    int val = 0;
    MPMCBoundedQueue<int> queue(2);
    ASSERT_TRUE(queue.enqueue(1));
    ASSERT_TRUE(queue.dequeue(val));
    ASSERT_EQ(val, 1);
    ASSERT_FALSE(queue.dequeue(val));

    ASSERT_TRUE(queue.enqueue(2));
    ASSERT_TRUE(queue.enqueue(3));
    ASSERT_FALSE(queue.enqueue(4));

    ASSERT_TRUE(queue.dequeue(val));
    ASSERT_EQ(val, 2);
    ASSERT_TRUE(queue.dequeue(val));
    ASSERT_EQ(val, 3);

    ASSERT_FALSE(queue.dequeue(val));
}

TEST(Correctness, NoSpuriousFails) {
    const int N = 1024 * 1024;
    const int nThreads = 4;
    MPMCBoundedQueue<int> queue(N * nThreads);

    std::vector<std::thread> writers;
    for (int i = 0; i < nThreads; i++) {
        writers.emplace_back([&] {
            for (int j = 0; j < N; ++j) {
                ASSERT_TRUE(queue.enqueue(0));
            }
        });
    }

    for (auto& t : writers) t.join();

    std::vector<std::thread> readers;
    for (int i = 0; i < nThreads; i++) {
        readers.emplace_back([&] {
            for (int j = 0; j < N; ++j) {
                int k;
                ASSERT_TRUE(queue.dequeue(k));
            }
        });
    }

    for (auto& t : readers) t.join();    
}

TEST(Correctness, NoQueueLock) {
    const int N = 1024 * 1024;
    const int nThreads = 8;
    MPMCBoundedQueue<int> queue(64);
 
    std::vector<std::thread> threads;
    std::atomic<int> ids = {0};
    for (int i = 0; i < nThreads; i++) {
        threads.emplace_back([&] {
            int id = ids++;
            if (id % 2) {
                for (int j = 0; j < N; ++j) {
                    queue.enqueue(0);
                }
            } else {
                for (int j = 0; j < N; ++j) {
                    int k;
                    queue.dequeue(k);
                }
            }
        });
    }
 
    for (auto& t : threads) t.join();
 
    int k;
    while (queue.dequeue(k)) {
        queue.dequeue(k);
    }
    ASSERT_TRUE(queue.enqueue(0));
    ASSERT_TRUE(queue.dequeue(k));
    ASSERT_EQ(k, 0);
}

