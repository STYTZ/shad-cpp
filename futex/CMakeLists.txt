cmake_minimum_required(VERSION 2.8)
project(futex)

if (TEST_SOLUTION)
  include_directories(../private/futex/)
endif()

include(../common.cmake)

add_gtest(test_futex test.cpp)
add_benchmark(bench_futex bench.cpp)
